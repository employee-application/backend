package com.inapp.employees.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.inapp.employees.exception.ResourceNotFoundException;
import com.inapp.employees.model.Employee;
import com.inapp.employees.repository.EmployeeRepository;

import jakarta.annotation.PostConstruct;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/v1")
public class EmployeeController 
{
	@Autowired
	EmployeeRepository repo;
	
	@GetMapping("/employees")
	public List<Employee> getAllEmployees()
	{
		return repo.findAll();		
	}
	
	@PostMapping("/add")
	public Employee addEmployee(@RequestBody Employee emp)
	{
		return repo.save(emp);
	}
	
	@GetMapping("/get/{id}")
	ResponseEntity<Employee> getEmployee(@PathVariable("id")Long eid)
	{
		Employee emp=repo.findById(eid)
				.orElseThrow(()->new ResourceNotFoundException("Employee not Existing for id "+eid));
		return ResponseEntity.ok(emp);
	}
	
	@PutMapping("update/{id}")
	ResponseEntity<Employee> updateEmployee(@PathVariable("id")Long eid,@RequestBody Employee emp)
	{
		Employee employee=repo.findById(eid)
				.orElseThrow(()->new ResourceNotFoundException("Employee not Existing for id "+eid));
		
		employee.setFname(emp.getFname());
		employee.setLname(emp.getLname());
		employee.setEmailId(emp.getEmailId());
		
		Employee newEmp=repo.save(employee);
		return ResponseEntity.ok(newEmp);
		
	}
	
	@DeleteMapping("delete/{id}")
	ResponseEntity<Map<String, Boolean>> deleteEmployee(@PathVariable("id") Long eid)
	{
		Employee employee=repo.findById(eid)
				.orElseThrow(()->new ResourceNotFoundException("Employee not Existing for id "+eid));
		repo.delete(employee);
		Map<String,Boolean> response=new HashMap<>();
		response.put("Deleted", Boolean.TRUE);
		return ResponseEntity.ok(response);
	}
	
	
}
