package com.inapp.employees;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootAngularBackEndApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootAngularBackEndApplication.class, args);
	}

}
